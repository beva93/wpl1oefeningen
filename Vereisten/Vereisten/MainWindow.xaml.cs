﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Vereisten
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void img_roos_MouseEnter(object sender, MouseEventArgs e)
        {
            Hoofdscherm.Background = Brushes.Red;
        }

        private void img_goudsbloem_MouseEnter(object sender, MouseEventArgs e)
        {
            Hoofdscherm.Background = Brushes.Orange;
        }

        private void img_lavendel_MouseEnter(object sender, MouseEventArgs e)
        {
            Hoofdscherm.Background = Brushes.Lavender;
        }

        private void img_wilkensbitter_MouseEnter(object sender, MouseEventArgs e)
        {
            Hoofdscherm.Background = Brushes.Yellow;
        }

        private void img_roos_MouseLeave(object sender, MouseEventArgs e)
        {
            Hoofdscherm.Background = Brushes.LightGray;
        }

        private void img_goudsbloem_MouseLeave(object sender, MouseEventArgs e)
        {
            Hoofdscherm.Background = Brushes.LightGray;
        }

        private void img_lavendel_MouseLeave(object sender, MouseEventArgs e)
        {
            Hoofdscherm.Background = Brushes.LightGray;
        }

        private void img_wilkensbitter_MouseLeave(object sender, MouseEventArgs e)
        {
            Hoofdscherm.Background = Brushes.LightGray;
        }
    }
}
